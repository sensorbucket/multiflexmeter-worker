FROM node:12 AS builder

WORKDIR /opt/app

COPY . .

RUN yarn install \
  && yarn build \
  && yarn install --production \
  && yarn cache clean

FROM sensorbucket/generic-worker:latest AS release

ENV MQ_QUEUE="worker/multiflexmeter"

COPY --from=builder /opt/app/node_modules/ /opt/app/worker/node_modules/
COPY --from=builder /opt/app/dist/ /opt/app/worker/