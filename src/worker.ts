import { Logger } from '@nestjs/common';
import {
  ETLInterface,
  ETLHelperInterface,
  UplinkMessage,
} from '@sensorbucket/models';

export default class MFMWorker implements ETLInterface {
  // This worker can only process uplink messages for the device multiflexmeter
  private readonly capabilities = ['uplink.device.mfm'];

  private logger: Logger;
  private helper: ETLHelperInterface;

  /**
   *
   * @param helper
   * @param logger
   */
  initialize(helper: ETLHelperInterface, logger: Logger): string[] {
    this.helper = helper;
    this.logger = logger;
    this.logger.log('ETL Script initialized');

    return this.capabilities;
  }

  /**
   * Fired when a new message for this worker has been received
   * @param message The received message
   */
  onMessage(message: UplinkMessage) {
    return this.parseUplink(message);
  }

  /**
   * Parses the uplink message
   * extracts the device-data
   * @param message The uplink message
   */
  async parseUplink(message: UplinkMessage) {
    const { deviceData } = message;

    const buffer = Buffer.from(deviceData);
    const waterHeight = buffer.readUInt16LE();

    this.logger.log(`MFM ${message.device} resulted in ${waterHeight} cm`);

    // TODO: publish measurement
  }
}
